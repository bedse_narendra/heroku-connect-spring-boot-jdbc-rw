package com.example;


public class Contact {
    private int id;
    private String sfid;
    private String first;
    private String last;
    private String department;
  
    public void setEmail(String email) {
        this.email = email;
    }
    public void setDepartment(String department) {
        this.department = department;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSfid(String sfid) {
        this.sfid = sfid;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setLast(String last) {
        this.last = last;
    }

    private String email;
    public Contact() {}
    public Contact(int id, String sfid, String first, String last, String email ,String department) {
        super();
        this.id = id;
        this.sfid = sfid;
        this.first = first;
        this.last = last;
        this.email = email;
        this.department=department;
       
    }
    public int getId()
    {
        return id;
    }

   
    public String getSfid()
    {
        return sfid;
    }

    public String getLast()
    {
        return this.last;
    }

    public String getDepartment()
    {
        return this.department;
    }

    public String getFirst()
    {
        return this.first;
    }

    public String getEmail()
    {
        return this.email;
    }
}
