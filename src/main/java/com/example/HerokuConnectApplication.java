package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import java.net.URISyntaxException;
import java.net.URI;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


@Controller
@SpringBootApplication
public class HerokuConnectApplication {

    @RequestMapping("/")
    public String home(Model model) {
        return "home";
    }

    @RequestMapping("/createcontactform")
    public String createContactForm(Model model) {
        model.addAttribute("contact", new Contact());
        return "createcontact";
    }
    
    

	@RequestMapping("/contacts")
    public String contacts(Model model) {
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();
            String sql;
            sql = "SELECT id, sfid,  firstname, lastname, name, email,department FROM salesforce.contact";
            ResultSet rs = stmt.executeQuery(sql);
            StringBuffer sb = new StringBuffer();
            List contacts = new ArrayList<>();
            // Extract data from result set
            while (rs.next()) {
                int id = rs.getInt("id");
                String sfid = rs.getString("sfid");
                String name = rs.getString("name");
                String first = rs.getString("firstname");
                String last = rs.getString("lastname");
                String email = rs.getString("email");
                String department = rs.getString("department");
                contacts.add(new Contact(id, sfid, first, last, email,department));
            }
            model.addAttribute("contacts", contacts);
            return "contact";
        } catch (Exception e) {
            return e.toString();
        }
    }

   
		@RequestMapping("/csvfile")
	    public String createCSVfile(Model model) {
	        
	    // String csvFile="/home/arcasan/Documents/WSO2ESB/heroku-test.csv";
	      String ftpURL = "74.208.178.64";
	     String username = "arcauser";
	     String password = "arcasan1";
	     int port = 22;
	     String sftpDownloadDir = "/home/Shopify_Folder/";
	     
	     Session session = null;
	           Channel channel = null;
	           ChannelSftp downloadChannelSftp = null;
	       
	        
	     try {
	      //create a object
	      JSch jsch = new JSch();
	      //creating a session object
	      session= jsch.getSession(username, ftpURL, port);
	      //setting a password
	      session.setPassword(password);
	      // Create a Properties object of Java Util
	      Properties config = new Properties();
	      config.put("StrictHostKeyChecking", "no");
	      session.setConfig(config);
	      //connecting to SFTP server
	      session.connect();
	      System.out.println("Host connected");
	      //creating a channel object for SFTP
	               channel = session.openChannel("sftp");
	               //connecting channel to SFTP Server
	               channel.connect();
	               System.out.println("sftp channel opened and connected.");
	               // Type Caste a Channel object to ChannelSftp Object
	               downloadChannelSftp = (ChannelSftp) channel;
	               // Go to particular directory using SftpChannel
	               downloadChannelSftp.cd(sftpDownloadDir);
	               //Print the current path on console
	               System.out.println("Path = " + downloadChannelSftp.pwd());
	             
	               //getting a file from  SFTP Server to local system
	               String fileName = "heroku-test.csv";
	               File downloadFile = new File(sftpDownloadDir + "/" + fileName);
	               InputStream inputStream = downloadChannelSftp.get(downloadFile.getName());
	               
	               DataInputStream in = new DataInputStream(inputStream); 
	               
	               
	               System.out.println("File Download successfully");
	              

	               							BufferedReader br=null;
							               String line = "";
							               String cvsSplitBy = ",";
							
							               try {
							
							            	    br = new BufferedReader(new InputStreamReader(in)); 
							                   
							                   try {
												while ((line = br.readLine()) != null) {

												       // use comma as separator
												       String[] country = line.split(cvsSplitBy);
												       System.out.println(country);
												      // int PK_acc_ID_result = Integer.parseInt(country[0]);
												  //  String   sfid=country[1];
												    String   first=country[0];
												    String   last=country[1];
												    String   email=country[2];
												    String  department=country[3];
												    String   name=country[4];
													String cnum=country[5];
												    createContactLocal(first,last,email,department,name,cnum);
												   }
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
							
							               }  finally {
							                   if (br != null) {
							                       try {
							                           br.close();
							                       } catch (IOException e) {
							                           e.printStackTrace();
							                 }
					        }
				   }
	             
			 } catch (JSchException exc) {
			  exc.printStackTrace();
			 } catch (SftpException exc) {
			  exc.printStackTrace();
			 }
			 finally {
			  downloadChannelSftp.exit();

			  channel.disconnect();
			  session.disconnect();
			 }
				return "CSVfile";
			}

	    
	    public void createContactLocal(String firstname,String lastname,String email,String department,String name,String cnum) {
	      
		  int i=alreadyAvail(cnum);
	    	if(i==0)
	    	{
	    	
				
									try {
										Connection connection = getConnection();
										Statement stmt = connection.createStatement();
										String sql3="INSERT INTO salesforce.account (name,arcm__external_id__c) VALUES ('"+name+"','"+cnum+"');";
										System.out.println(" Insert SQL: " + sql3);
										int result3 = stmt.executeUpdate(sql3);
										String sql4="INSERT INTO salesforce.contact (firstname, lastname,email,department, account__arcm__external_id__c)SELECT '"+firstname+"', '"+lastname+"','"+email+"','"+department+"', arcm__external_id__c FROM salesforce.account where id=(select MAX(id) from salesforce.account);";
										System.out.println(" INSERT SQL: " + sql4);
     									int rlt4 = stmt.executeUpdate(sql4);
										
										connection.close();
									}catch(Exception e){
										e.printStackTrace();

									}
			}
			else
			{
			
								try {
										Connection connection = getConnection();
										Statement stmt = connection.createStatement();
										String sql1="UPDATE salesforce.account set name='"+name+"' where arcm__external_id__c = '"+cnum+"';";
									   int result1 = stmt.executeUpdate(sql1);
									   System.out.println("Update SQL: " + sql1);
									   String sql2="UPDATE salesforce.contact set firstname='"+firstname+"',lastname='"+lastname+"',email='"+email+"',department='"+department+"' where account__arcm__external_id__c = '"+cnum+"';";
									   System.out.println("update SQL: " + sql2);
								       int rslt2 = stmt.executeUpdate(sql2);
									   connection.close();
									}catch(Exception e){
										e.printStackTrace();

									}
			
			
			}
	        
	    }
	     public static Integer alreadyAvail(String cnum)
		{
	    	int rowCount=0;
			
	    	 try {
	             Connection connection = getConnection();
	             Statement stmt = connection.createStatement();
	            
	             ResultSet rs = stmt.executeQuery("Select count(*) from salesforce.account where arcm__external_id__c='"+cnum+"';");
	             rs.next();
	             rowCount = rs.getInt(1);
	             
	    	 } catch (Exception e) {
	             
	         }
	    	return rowCount;
				
		}
	
	
    private static Connection getConnection() throws URISyntaxException, SQLException {
        URI dbUri = null;
        String DEFAULT_DATABASE_URL = "postgres://u:p" +
                "@ec2-xx.compute-1.amazonaws.com:5432/db";
        try {
            dbUri = new URI(System.getenv("DATABASE_URL"));
        }catch (Exception e){
            e.printStackTrace();
            dbUri = new URI(DEFAULT_DATABASE_URL);
        }

		String username = dbUri.getUserInfo().split(":")[0];
		String password = dbUri.getUserInfo().split(":")[1];
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':'
                + dbUri.getPort() + dbUri.getPath()
                + "?sslmode=require";

		return DriverManager.getConnection(dbUrl, username, password);
	}

	public static void main(String[] args) {
		SpringApplication.run(HerokuConnectApplication.class, args);
	}
}
